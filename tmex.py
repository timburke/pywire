#tmex.py
#A python ctypes wrapper around the Maxim 1-wire TMEX API

from ctypes import *
from ctypes.util import find_library

from tmsession import TMSession

class TMEX:
	libname = 'Ibfs32'
	
	def __init__(self):
		self.dll = WinDLL(find_library('Ibfs32'))
		
		self._load_prototypes()
	
	def _proto(self, name, args, ret):
		setattr(self, name, getattr(self.dll, name))
		
		func = getattr(self, name)
		
		func.argtypes = args
		func.restype = ret
	
	def _load_prototypes(self):
		"""
		Load in all the functions from the dll as ctypes foreign functions
		"""
		
		#Session Management
		self._proto("TMExtendedStartSession", [c_short, c_short, c_void_p], c_long)
		self._proto("TMStartSession", [c_short], c_long)
		self._proto("TMValidSession", [c_long], c_short)
		self._proto("TMEndSession", [c_long], c_short)
		self._proto("TMReadDefaultPort", [POINTER(c_short), POINTER(c_short)], c_short)
		
		#Hardware Functions
		self._proto("TMSetup", [c_long], c_short)
		self._proto("TMFirst", [c_long, c_void_p], c_short)
		self._proto("TMNext", [c_long, c_void_p], c_short)
		self._proto("TMRom", [c_long, c_void_p, POINTER(c_short)], c_short)
		self._proto("TMAccess", [c_long, c_void_p], c_short)
		self._proto("TMStrongAccess", [c_long, c_void_p], c_short)
		self._proto("TMTouchReset", [c_long], c_short)
		self._proto("TMOneWireLevel", [c_long, c_short, c_short, c_short], c_short)
		
		#Transport Functions
		self._proto("TMCRC", [c_short, POINTER(c_ubyte), c_ushort, c_short], c_short)
		self._proto("TMTouchByte", [c_long, c_short], c_short)
		self._proto("TMTouchBit", [c_long, c_short], c_short)
		
	#High level api functions
	def default_port(self):
		"""
		Get the default port settings for this computer
		"""
		
		port = c_short()
		type = c_short()
		
		err = self.TMReadDefaultPort(byref(port), byref(type))
		
		if err != 1:
			raise ValueError("Error reading default port: %d" % err.value)
			
		return (port.value, type.value)
		
	def create_session(self, port, type):
		"""
		Create a 1-wire session with the given port and type
		"""
		
		cport = c_short(port)
		ctype = c_short(type)
		
		session = self.TMExtendedStartSession(cport, ctype, None)
		
		if session < 0:
			raise EnvironmentError(session.value, "Error creating session")
		elif session == 0:
			raise ValueError("Port is in use by another application")
		
		return TMSession(self, session)
		
	def default_session(self):
		"""
		Create a session based on the default port information
		"""
		
		(port, type) = self.default_port()
		
		return self.create_session(port, type)