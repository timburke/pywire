#device.py
#A base class representing a device on the 1-wire bus

class OWDevice (object):
	def __init__(self, sess, info, type="unknown", name="unnamed"):
		if not self._family_matches(info["family"]):
			raise ValueError("Invalid device type for this device driver class")
		
		self.info = info
		self.type = type #A string representing the type of this device
		self.name = name #A string with the informative name of this device
		self.sess = sess
		
	def _family_matches(self, family):
		"""
		Override this routine in subclasses to only match certain device classes
		"""
		return True
	
	def select(self, strong=False):
		"""
		Select this device for a subsequent command.  If strong is passed as True, then the presence of this device is explicitly verified
		"""
		
		if strong:
			self.sess.strong_access(self.info["rom"])
		else:
			self.sess.access(self.info["rom"])
	
	def present(self):
		"""
		Return true or false if this device is present or not on the network.
		"""
		
		try:
			self.select(strong=True)
		except ValueError:
			return False #We get a ValueError only when the command succeeded but there was no device with a matching serial
			
		return True