#ds18B20.py
#A 1-wire device class for the ds18b20 digital temperature measurement device

import device
import time
import array

class DS18B20 (device.OWDevice):
	def __init__(self, sess, info, name):
		super(DS18B20, self).__init__(sess, info, "DS18B20 Digital Temperature Sensor", name)
	
	def _family_matches(self, family):
		return family == 0x28
	
	def read_scratchpad(self, strong=False):
		"""
		Read 8 byte scratchpad and make sure the CRC checks
		If strong is True, then make sure this device is present
		"""
		
		mem = array.array('i', (0 for i in xrange(0,8)))
		
		try: 
			self.select(strong)
			
			#Write the read scratchpad command
			self.sess.write_byte(0xBE)
			
			#Read in the 8 scratchpad bytes
			crc = 0
			for i in xrange(0, 8):
				mem[i] = self.sess.read_byte()
				crc = self.sess.crc_byte(mem[i], crc)
			
			#Read in the crc and make sure it matches
			dev_crc = self.sess.read_byte()
			
			if crc != dev_crc:
				raise IOError((-1, "Invalid CRC for scratchpad (received %d, calculated %d)" % (dev_crc, crc)))
				
		finally:
			self.sess.reset()
		
		return mem
	
	def start_conversion(self, broadcast=False, strong=False, sync=False):
		"""
		Tell this device to start a temperature conversion.  If broadcast is True, 
		this request is sent to all DS18B20 devices on the network
		"""
		
		if broadcast:
			select_fun = lambda: self.sess.write_byte(0xCC) #Skip rom check step
		else:
			select_fun = lambda: self.select(strong) 
		
		try:
			#Make sure the scratchpad has correct config data
			select_fun()
			self.sess.write_byte(0xB8)
			
			#Send conversion command
			select_fun()
			self.sess.write_byte(0x44)
			
			#sleep in 200 ms increments and check to see if the conversion is done
			sleeps = 0
			if sync:
				while True:
					time.sleep(0.2)
					sleeps += 1
					if self.sess.transfer_bit(1) == 1:
						#device responds with a 1 once it is done converting the temperature
						break
				
				#print "Slept for %d ms" % (sleeps*200)
		except:
			self.sess.reset()
			raise
	
	def _convert_temperature(self, mem, units):
		"""
		Convert the temperature from the internal format to a number
		"""
		
		sgn_mask = (0b1 << 11)
		print "Sign mask", bin(sgn_mask)
		
		raw_temp = mem[1] << 8 | mem[0]
		print "LSB", bin(mem[0])
		print "MSB", bin(mem[1])
		
		print "Raw temp", bin(raw_temp)
		sign = (raw_temp & sgn_mask) >> 10 #extract the sign extended portion
		print "sign part", bin(sign)
		
		raw_temp = raw_temp & ~sgn_mask #mask out sign
		print "Masked temp", bin(raw_temp)
		
		#Negative
		if sign == 0b1:
			sign = -1
			raw_temp = ~ raw_temp
			raw_temp += 1
		else:
			sign = 1
			
		precision = mem[4] & (0b11 << 4) >> 4
		

		#Decide how many bits to mask out
		prec_mask = 0
		
		if precision == 0:
			prec_mask = 0b111
		elif precision == 0b01:
			prec_mask = 0b11
		elif precision == 0b10:
			prec_mask = 0b1
		
		raw_temp ^= prec_mask
		
		temp = sign*raw_temp*0.0625 #in celsius
		
		if units == 'fahrenheit':
			temp = 9./5.*temp + 32.0
		
		return temp
		
	def get_temperature(self, strong=False, units="celsius"):
		"""
		Start a temperature conversion, wait for it to finish and then report back the temperature
		"""
		self.start_conversion(strong=strong, sync=False)
		time.sleep(1.0)
		
		return self.read_temperature(strong)
		
	def read_temperature(self, strong=False, units="celsius"):
		"""
		Read in a previously converted temperature and return it in the chosen units
		"""
		mem = self.read_scratchpad(strong)
		
		return self._convert_temperature(mem, units)