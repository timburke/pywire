#!/usr/bin/python

from distutils.core import setup


setup(	name='pywire',
		version='1.0',
		description='Python wrapper around Maxim 1-wire TMEX Drivers on Windows.',
		author='Tim Burke',
		author_email='timburke@stanford.edu',
		py_modules=['pywire']
	)