#tmsession.py
#A class representing a 1-wire session in TMEX

from ctypes import *
import ds18b20
import device

class TMSession:
	def __init__(self, tm, handle):
		"""
		Wrap a session handle with this object
		"""
		
		self.tm = tm
		self.handle = handle
		self.state_buffer = create_string_buffer(5120) #This must be 5K for non EPROM and 15K for EPROM devices per http://files.maximintegrated.com/sia_bu/softdev/owdocs/Docs/TMEX/tmwi5ur7.html
		
		self.driver_map = {}
		self.driver_map[0x28] = ds18b20.DS18B20
		
		try:
			self._setup()
		except EnvironmentError:
			self.close()
			raise
	
	def close(self):
		err = self.tm.TMEndSession(self.handle)
		
		if err < 0:
			raise EnvironmentError((err, "Error closing session handle"))
		elif err == 0:
			raise ValueError("Session already closed")
	
	def _setup(self):
		"""
		Internal setup function called to prepare the bus for use.
		"""
		
		err = self.tm.TMSetup(self.handle)
		
		#Check for success
		if err == 1:
			return
		
		#Report the error
		raise EnvironmentError((err, "Error setting up 1-wire network"))
		
	def _make_serial(self, rom):
		"""
		Convert serial number into hex string
		"""
		
		return "".join(map(lambda x: "{0:02x}".format(x), rom))
		
	def read_device_info(self):
		"""
		Read the 8-byte serial code from the last device enumerated
		"""
		
		RomType = c_short * 8
		
		rom_arr = RomType(0, 0, 0, 0, 0, 0, 0, 0)
		
		err = self.tm.TMRom(self.handle, self.state_buffer, rom_arr)
		if err < 0:
			raise EnvironmentError((err, "Error reading ROM from device"))
		
		rom = map(lambda x: x, rom_arr)
		
		info = {}
		
		info["rom"] = rom
		info["serial"] = self._make_serial(rom)
		info["family"] = rom[0]
		
		return info
	
	def get_next_device(self):
		"""
		Continue the search algorithm with the next device in the search
		"""
		
		err = self.tm.TMNext(self.handle, self.state_buffer)
		
		if err == 0:
			raise ValueError("No devices on the network")
		elif err < 0:
			raise EnvironmentError((err, "Error querying first device on the network"))
		
		return self.read_device_info()
	
	def get_first_device(self):
		err = self.tm.TMFirst(self.handle, self.state_buffer)
		
		if err == 0:
			raise ValueError("No devices on the network")
		elif err < 0:
			raise EnvironmentError((err, "Error querying first device on the network"))
		
		return self.read_device_info()
	
	def write_serial(self, rom):
		"""
		Write serial number to internal buffer in TMEX driver
		"""
		
		RomType = c_short * 8
		
		rom_arr = RomType()
		
		for i in xrange(0,8):
			rom_arr[i] = rom[i]
		
		err = self.tm.TMRom(self.handle, self.state_buffer, rom_arr)
		if err < 0:
			raise EnvironmentError((err, "Error reading ROM from device"))
	
	def set_level(self, level, delay):
		primed_map = {"next byte": 2, "next bit": 1, "now": 0}
		level_map = {"strong": 1, "normal": 0, "break": 2, "program": 3}
		
		primed = primed_map[delay]
		lev = level_map[level]
		
		ret = self.tm.TMOneWireLevel(self.handle, 0, lev, primed)
	
		if ret >= 0:
			return
		else:
			raise EnvironmentError((ret, "Error setting One Wire Level"))
	
	def access(self, rom):
		"""
		Select the specified device serial number for access if present.
		This does not guarentee that the device is present, only that a device is present and this
		one, if present, is selected. 
		
		rom should be an array of 8 integers corresponding to the 64bit serial number of the device
		that should be accessed.
		"""
		
		self.write_serial(rom)
		err = self.tm.TMAccess(self.handle, self.state_buffer)
	
		if err == 0:
			return ValueError("No devices on the network")
		elif err < 0:
			raise EnvironmentError((err, "Error accessing device"))
	
	def strong_access(self, rom):
		"""
		Select the specified device serial number for access, verifying that the device is present.

		rom should be an array of 8 integers corresponding to the 64bit serial number of the device
		that should be accessed.
		"""
		
		self.write_serial(rom)
		err = self.tm.TMStrongAccess(self.handle, self.state_buffer)
	
		if err == 0:
			return ValueError("Device with given serial number is not present")
		elif err < 0:
			raise EnvironmentError((err, "Error strong accessing device"))
	
	def transfer_byte(self, byte):
		"""
		Read and write one byte on the 1-wire network
		"""
		
		ret = self.tm.TMTouchByte(self.handle, c_short(byte))
		
		if ret < 0:
			raise EnvironmentError((err, "Error transfering data to device"))
		
		return ret
		
	def transfer_bit(self, bit):
		"""
		Write and then read 1 bit from the 1-wire network
		"""
		ret = self.tm.TMTouchBit(self.handle, c_short(bit))
		
		if ret < 0:
			raise EnvironmentError((err, "Error transfering data to device"))
		
		return ret
	
	def read_byte(self):
		return self.transfer_byte(0xFF)
	
	def write_byte(self, b):
		return self.transfer_byte(b)

	def crc_byte(self, b, curr):
		"""
		Perform incremental CRC on the next byte of data as if it were appended to whatever array
		was used to calculate the current CRC value curr
		"""
		
		ub = c_ubyte(b)
		
		ret = self.tm.TMCRC(1, byref(ub), curr, 0)
		
		if ret < 0:
			raise EnvironmentError((err, "Error calculating CRC value"))
		
		return ret #new CRC value
		
	def reset(self):
		"""
		Reset all devices on the 1-wire network
		"""
		
		err = self.tm.TMTouchReset(self.handle)
		
		if err == 1 or err == 2:
			return
		
		raise EnvironmentalError((err, "Error resetting the bus"))
		
	def enumerate(self):
		"""
		Get all the devices on this network.
		"""
		
		found = []
		count = 1
		
		try:
			while True:
				info = self.get_next_device()
				name = "Device %d" % count
				
				#Load the correct device class for this device or a generic one if we don't have a driver
				if info["family"] in self.driver_map:
					f = self.driver_map[info["family"]](self, info, name)
				else:
					f = device.OWDevice(self, info, name=name)
								
				found.append(f)
				count += 1
		except ValueError:
			pass
		
		return found